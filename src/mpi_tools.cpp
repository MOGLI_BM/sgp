/*
    Useful wrappers for MPI domain

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#include "mpi_tools.hpp"



namespace sgp {
namespace mpi {

#ifdef SGP_COMPILE_WITH_MPI // ======================================

/* Find all combinations of process distributions for the three directions: dof, mod_1 and mod_2
    @param np_total     total number of processes
    @param min          minimum number of processes that shall be used for each direction
    @param max          maximum number of processes that shall be used for each direction
    @param strategy     strategy regarding domain decomposition, i.e. maximize or minimize decomposition in dof-direction

    @return all possible domain decompositions s.th. np_total is completely used and neither of the limits are exceeded
*/
std::vector<NP> find_all(const int np_total, const NP& min, const NP& max, NP_dof strategy)
{
    std::vector<NP> all;

    for (int i = (strategy == MINIMIZE) ? min.dof : max.dof; min.dof <= i && i <= max.dof; i -= strategy)
    {
        for (int j = min.mod_1, ij = i*min.mod_1; j <= max.mod_1 && ij <= np_total; ++j, ij += i)
        {
            int k = np_total / ij;

            if (ij * k == np_total && min.mod_2 <= k && k <= max.mod_2)
            {
                all.push_back(NP(i, j, k));
            }
        }
    }

    return all;
}

/* Compute distribution of MPI processes
    @param comm         global MPI communicator
    @param n_dof        number of degrees of freedom
    @param n_mod_1      number of load cases of first kind
    @param n_mod_2      number of load cases of second kind
    @param strategy     strategy regarding domain decomposition, i.e. maximize or minimize decomposition in dof-direction
    @param omega        weighting factor [0,∞) between strategy and load balancing:
                        smaller omega favor better load balancing, while larger omega favor distributions which follow
                        the given strategy more strictly
                        omega = 0 -> ignore the strategy
                        warning: omega > 1 may result in very poor load balancing
    @param min          minimum number of processes that shall be used for each direction
    @param max          maximum number of processes that shall be used for each direction
    @return number of MPI-processes in dof, mod_1 and mod_2 direction, as well as the effectively used number of processes
*/
NP distribute(const MPI_Comm comm, int n_dof, int n_mod_1, int n_mod_2, NP_dof strategy, double omega, NP& min, NP& max)
{
    int np_total;
    MPI_Comm_size(comm, &np_total);

    if (np_total > (max.dof * max.mod_1 * max.mod_2))
    {
        throw std::runtime_error("To many MPI Processes for domain decomposition!");
    }

    min.dof     = std::max(1, min.dof);
    max.dof     = std::min(n_dof, max.dof);
    min.mod_1   = std::max(1, min.mod_1);
    max.mod_1   = std::min(n_mod_1, max.mod_1);
    min.mod_2   = std::max(1, min.mod_2);
    max.mod_2   = std::min(n_mod_2, max.mod_2);

    auto all = find_all(np_total, min, max, strategy);

    NP result = all[0];//((strategy == MINIMIZE)? max.dof : min.dof,0,0,1e-16);

    for (auto& np : all)
    {
        // if (np.dof <= max.dof && np.mod_1 <= max.mod_1 && np.mod_2 <= max.mod_2
            // && np.dof >= min.dof && np.mod_1 >= min.mod_1 && np.mod_2 >= min.mod_2)
        // {
            // number of effectively used processes
            double eff1 = n_mod_1 / std::ceil(n_mod_1 / double(np.mod_1));
            double eff2 = n_mod_2 / std::ceil(n_mod_2 / double(np.mod_2));
            np.effective = np.dof * eff1 * eff2;

            // effectivity improvement of np vs result
            double eff_incr = np.effective/result.effective;
            // strategy loss of np vs result
            double strat_loss = (strategy == MINIMIZE)? np.dof/result.dof : result.dof/np.dof;

            if (eff_incr > std::max(omega*strat_loss, 1.0))
                result = np;
        // }
    }

    if (result.effective < 0)
    {
        throw std::runtime_error("No valid combination of processes available!");
    }

    return result;
}


DomainDecomposition::DomainDecomposition(const MPI_Comm comm_, const NP np)
{
    MPI_params tmp(comm_);

    if (tmp.np != np.dof * np.mod_1 * np.mod_2)
    {
        throw std::runtime_error("Number of total MPI Processes must be equal to np.dof * np.mod_1 * np.mod_2!");
    }

    world.set_comm(comm_, np.mod_1*np.mod_2, np.dof);

    MPI_Comm tmp_comm;
    int rows[2] = {1,0};
    int cols[2] = {0,1};

    MPI_Cart_sub(world.comm, rows, &tmp_comm);
    mod.set_comm(tmp_comm, np.mod_1, np.mod_2);

    MPI_Cart_sub(world.comm, cols, &tmp_comm);
    dof.set_comm(tmp_comm);

    assert(mod.np == np.mod_1*np.mod_2 && dof.np == np.dof);
}

#endif // ====================================== SGP_COMPILE_WITH_MPI

/* compute local part of an array of size N for all processes
    @param np   number of MPI processes in the communicator
    @param N    global array size

    @return vector of local starting and end indices and array sizes
*/
std::vector<LocalIndex> compute_local_indices(int np, int N)
{
    std::vector<LocalIndex> idx(np);

    for (int i = 0; i < np; ++i)
    {
        idx[i].size = N / np;

        if (i < N % np)
        {
            idx[i].size += 1;
        }

        idx[i].end = idx[i].begin + idx[i].size;

        if (i < np - 1)
        {
            idx[i+1].begin = idx[i].end;
        }
    }

    return idx;
}


} // namespace mpi
} // namespace sgp

