## C++ template library for solving optimization problems using sequencial global programming (SGP)

The algorithms are parallelized with MPI and have been shown to be suitable for HPC-systems.
Fully templated data structures enable usage in a wide field of applications and discretization schemes.

A example implementation of the algorithm, where SGP is used for optimizing optical properties of nanoparticles can be found here:
https://gitlab.com/MOGLI_BM/sgp-dda
