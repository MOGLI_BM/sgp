/*
   Functions to test the correctness of a separable model

   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include "functionvalue.hpp"
#include "sgp.hpp"

namespace sgp {

// todo add check for sumS/sumJ
// todo param index
/* validate separable model by checking if S is a first order approximation of J
   @param  alg         the algorithm containing the model, material data etc.
   @param  edge_idx    index of the edge, along which both S and J will be evaluated
   @param  k1          index of parameter of first kind
   @param  k2          index of parameter of second kind
   @param  n           number of evaluations
   @returns    rho_i and evaluations of J(u-u1+vi*e1) and S[u](vi) (without penalty) for i=0,...,n-1
*/
template <class MAT_t, class SOL_t, class MOD_1_t, class MOD_2_t, int INTERPOLATION>
std::vector<Eval_JS>
    gradient_check_S(Algorithm<MAT_t, SOL_t, MOD_1_t, MOD_2_t, INTERPOLATION>& alg, size_t edge_idx, int k1, int k2, size_t n)
{
   // don't call this function with MPI!
   if (alg.dd.world.np > 1)
   {
      if (alg.dd.world.rnk == 0)
         std::cerr << "ERROR: gradient_check_S() should not be called with more than 1 MPI process!\n";
      exit(1);
   }

   // aliases
   auto& u     = alg.u_old[k1];
   auto& u_new = alg.u_new[k1];
   auto& P_new = alg.P[k1][k2];
   auto& Q     = alg.Q[k1][k2];
   auto& m1    = alg.mods1[k1];
   auto& m2    = alg.mods2[k2];
   auto& edge  = alg.edge_coeffs[edge_idx][k1];

   // initialize result
   std::vector<Eval_JS> result;

   // initialize u
   Arr1D<size_t> u0(alg.N, alg.edges[edge_idx][0]);
   alg.get_configuration(u0, alg.u_old);
   u_new = u;

   // initial solution, required for S
   double J0 = alg.eval(u, P_new, m1, m2);
   auto   P  = P_new;
   alg.eval_adj(u, Q, m1, m2);
   // alg.reorder_solution_vectors(alg.P, alg.P_T, 1);
   // alg.reorder_solution_vectors(alg.Q, alg.Q_T, 2);

   // set penalty to 0
   double tau   = 0;
   double j_pen = 0;
   double reg   = 0;

   // coordinate on edge and stepsize
   double rho  = 0;
   double dRho = 1.0 / double(n - 1);

   // evaluate J and S
   for (size_t k = 0; k < n; ++k)
   {
      // compute vi
      rho = dRho * k;
      MAT_t v;
      sgp::parametrization::Polynomial<INTERPOLATION>::eval(edge, v, rho);

      // set u0 = v
      u_new[0] = v;

      // j_pen = alg.J_pen(rho);

      // evaluate J and S
      double J = alg.eval(u_new, P_new, m1, m2);
      double S = alg.S(u[0], v, P[0], Q[0], j_pen, m1, m2, tau, 0, k1, reg);

      result.push_back({rho, J, S - reg});
   }

   // // evaluate gradients

   // result[0].dJ = (result[1].J - result[0].J)/dRho;
   // result[0].dS = (result[1].S - result[0].S)/dRho;
   // result[n-1].dJ = (result[n-1].J - result[n-2].J)/dRho;
   // result[n-1].dS = (result[n-1].S - result[n-2].S)/dRho;

   // for (size_t k = 1; k < n-1; ++k)
   // {
   //     result[k].dJ = (result[k+1].J - result[k-1].J)/(2*dRho);
   //     result[k].dS = (result[k+1].S - result[k-1].S)/(2*dRho);
   // }

   return result;
}

} // namespace sgp