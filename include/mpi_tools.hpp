/*
    Useful wrappers for MPI domain

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#pragma once

#include <array>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <cassert>

#ifdef SGP_COMPILE_WITH_MPI
#include <mpi.h>
#endif // SGP_COMPILE_WITH_MPI

namespace sgp {
namespace mpi {

#ifdef SGP_COMPILE_WITH_MPI

// minimize or maximize number of ranks used for dof parallelization
enum NP_dof {MINIMIZE = -1, MAXIMIZE = 1};

/* stores the number of MPI-processes used for parallelization
    in three different dimensions:
    degrees of freedom and load modifiers of first and second
    kind.
*/
struct NP
{
    int dof;
    int mod_1;
    int mod_2;

    /* effective number of processes.
        e.g. let n_dof = 10, NP.dof = 5,...,9 => NP.effective = 5,
        since the rank(s) responsible for 2 dof pose a bottleneck!
    */
    double effective;

    NP(int np_dof, int np_mod1, int np_mod2, double eff = 0.0)
        : dof(np_dof), mod_1(np_mod1), mod_2(np_mod2), effective(eff)
    {}
};

/* Find all combinations of process distributions for the three directions: dof, mod_1 and mod_2
    @param np_total     total number of processes
    @param min          minimum number of processes that shall be used for each direction
    @param max          maximum number of processes that shall be used for each direction
    @param strategy     strategy regarding domain decomposition, i.e. maximize or minimize decomposition in dof-direction

    @return all possible domain decompositions s.th. np_total is completely used and neither of the limits are exceeded
*/
std::vector<NP> find_all(const int np_total, const NP& min, const NP& max, NP_dof strategy);

/* Compute distribution of MPI processes
    @param comm         global MPI communicator
    @param n_dof        number of degrees of freedom
    @param n_mod_1      number of load cases of first kind
    @param n_mod_2      number of load cases of second kind
    @param strategy     strategy regarding domain decomposition, i.e. maximize or minimize decomposition in dof-direction
    @param omega        weighting factor [0,∞) between strategy and load balancing:
                        smaller omega favor better load balancing, while larger omega favor distributions which follow
                        the given strategy more strictly
                        omega = 0 -> ignore the strategy
                        warning: omega > 1 may result in very poor load balancing
    @param min          minimum number of processes that shall be used for each direction
    @param max          maximum number of processes that shall be used for each direction
    @return number of MPI-processes in dof, mod_1 and mod_2 direction, as well as the effectively used number of processes
*/
NP distribute(const MPI_Comm comm, int n_dof, int n_mod_1, int n_mod_2, NP_dof strategy, double omega, NP& min, NP& max);

struct MPI_params
{
    MPI_Comm comm;
    int np;
    int rnk;

    MPI_params(){}

    MPI_params(const MPI_Comm comm_)
    {
        set_comm(comm_);
    }

    void set_comm(const MPI_Comm comm_)
    {
        comm = comm_;
        MPI_Comm_size(comm, &np);
        MPI_Comm_rank(comm, &rnk);
    }
};

struct MPI_cart_params : public MPI_params
{
    int np_1, np_2;
    int coord[2];

    MPI_cart_params(){}

    MPI_cart_params(const MPI_Comm comm_, const int np_1_, const int np_2_)
    {
        set_comm(comm_, np_1_, np_1_);
    }

    void set_comm(const MPI_Comm comm_, const int np_1_, const int np_2_)
    {
        MPI_params::set_comm(comm_);
        np_1 = np_1_;
        np_2 = np_2_;

        assert(np == np_1*np_2);

        MPI_Comm cartcomm;
        int periods[2] {0,0};
        int dims[2] {np_1, np_2};

        MPI_Cart_create(comm, 2, dims, periods, 0, &cartcomm);
        MPI_params::set_comm(cartcomm);
        MPI_Cart_coords(comm, rnk, 2, coord);
    }

    std::array<int,2> rank_to_coords(int rank) const
    {
        int coord[2];
        MPI_Cart_coords(comm, rank, 2, coord);
        return {coord[0],coord[1]};
    }
};

/* stores different mpi-communicators for horizontal, vertical
    and global decomposition

*/
struct DomainDecomposition
{
    MPI_cart_params   world;    // global communicator where the dof are divided along the columns, the mods are divided along the rows
    MPI_params        dof;      // one column of world, i.e., all dof divided across the dof.np processes
    MPI_cart_params   mod;      // one row of world, i.e., all mods divided across the mod.np processes

    DomainDecomposition(){}

    /* Configure MPI environment
        @param comm_    global MPI communicator
        @param np       desired distribution of processes for the different directions (dof, mod_1, mod_2)
    */
    DomainDecomposition(const MPI_Comm comm_, const NP np);
};

#endif // SGP_COMPILE_WITH_MPI

struct LocalIndex
{
    int begin;
    int end;
    int size;
};

/* compute local part of an array of size N for all processes
    @param np   number of MPI processes in the communicator
    @param N    global array size

    @return vector of local starting and end indices and array sizes
*/
std::vector<LocalIndex> compute_local_indices(int np, int N);

} // namespace mpi
} // namespace sgp

