/*
    Functionality for parametrization of curves

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#pragma once
#include <array>

namespace sgp {
namespace parametrization {

/* provides functions to interpolate and evaluate polynomial interpolation
    @tparam Q       polynomial degree
*/
template <int Q>
struct Polynomial
{
    template <class T>
    using Nodes = std::array < T, Q + 1 >;

    /* compute parametrization of curve and store coefficients
        @param  data    curve data for rho = 0/Q,1/Q,...,Q/Q
        @param  coeffs  reference to the data where the coefficients of the parametrization will be stored
    */
    template <class T>
    static void interpolate(const Nodes<T>& data, Nodes<T>& coeffs);

    /* evaluate parametrization p for given parameter rho
        @param  coeffs  coefficients of parametrization p
        @param  result  reference to the data where the result will be stored
        @param  rho     in [0,1];
        @return x = p(rho)
    */
    template <class T>
    static void eval(const Nodes<T>& coeffs, T& result, const double rho);

};

template <int Q>
template <class T>
inline void Polynomial<Q>::eval(const Nodes<T>& coeffs, T& result, const double rho)
{
    result = coeffs[0];
    double rho_pow_i = 1;

    for (int i = 1; i <= Q; ++i)
    {
        rho_pow_i *= rho;
        result += rho_pow_i * coeffs[i];
    }
}

// **** LINEAR INTERPOLATION ****
template <>
template <class T>
inline void Polynomial<1>::interpolate(const Nodes<T>& data, Nodes<T>& coeffs)
{
    coeffs[0] = data[0];
    coeffs[1] = data[1] - data[0];
}

// **** QUADRATIC INTERPOLATION ****
template <>
template <class T>
inline void Polynomial<2>::interpolate(const Nodes<T>& data, Nodes<T>& coeffs)
{
    coeffs[0] = + data[0];
    coeffs[1] = - 3.000000 * data[0] + 4.000000 * data[1] - data[2];
    coeffs[2] = + 2.000000 * data[0] - 4.000000 * data[1] + 2.000000 * data[2];
}

// **** CUBIC INTERPOLATION ****
template <>
template <class T>
inline void Polynomial<3>::interpolate(const Nodes<T>& data, Nodes<T>& coeffs)
{
    coeffs[0] = + data[0];
    coeffs[1] = - 5.500000 * data[0] + 9.000000 * data[1] - 4.500000 * data[2] + 1.000000 * data[3];
    coeffs[2] = + 9.000000 * data[0] - 22.500000 * data[1] + 18.000000 * data[2] - 4.500000 * data[3];
    coeffs[3] = - 4.500000 * data[0] + 13.500000 * data[1] - 13.500000 * data[2] + 4.500000 * data[3];
}

} // namespace parametrization
} // namespace sgp

