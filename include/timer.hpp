/*
    Measure execution times for performance comparison

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <map>
#include <chrono>
#include <assert.h>


#ifdef SGP_COMPILE_WITH_MPI
    #include <mpi.h>
#endif // SGP_COMPILE_WITH_MPI

namespace sgp {

class Timer
{
  public:
    Timer(
        #ifdef SGP_COMPILE_WITH_MPI
        const MPI_Comm comm_ = MPI_COMM_WORLD,
        bool synchronized = true
        #endif // SGP_COMPILE_WITH_MPI
    )
        : t_total(std::chrono::duration<double>::zero())
        , t_section(std::chrono::duration<double>::zero())
        , running(false)
          #ifdef SGP_COMPILE_WITH_MPI
        , comm(comm_)
        , sync(synchronized)
          #endif // SGP_COMPILE_WITH_MPI
    {}

    /* reset all values to initial state
    */
    inline void reset()
    {
        assert(!(running));

        t_total   = std::chrono::duration<double>::zero();
        t_section = std::chrono::duration<double>::zero();
    }

    /* begin new timing section
        @param  resume whether timing shall be resumed immediately
        @returns duration of previous section
    */
    inline double new_section(bool resume = false)
    {
        assert(!(running));

        #ifdef SGP_COMPILE_WITH_MPI
        if (sync) MPI_Barrier(comm);
        #endif

        double d = time_section();
        t_section = std::chrono::duration<double>::zero();

        if (resume) start();

        return d;
    }

    /* start/resume timing after calling MPI_Barrier()
    */
    inline void start()
    {
        assert(!(running));

        #ifdef SGP_COMPILE_WITH_MPI
        if (sync) MPI_Barrier(comm);
        #endif

        running = true;
        t0      = std::chrono::high_resolution_clock::now();
    }

    /* stop/pause timing after calling MPI_Barrier()
        @returns time in seconds since hitting start()
    */
    inline double stop()
    {
        assert(running);

        #ifdef SGP_COMPILE_WITH_MPI
        if (sync) MPI_Barrier(comm);
        #endif

        t_part      = std::chrono::high_resolution_clock::now() - t0;
        t_total     += t_part;
        t_section   += t_part;
        running     = false;

        return t_part.count();
    }

    /* get total time
        @return sum of all timing sections in seconds
    */
    inline double time_total() const
    {
        return t_total.count();
    }

    /* get time of current section
        @return duration of current timing section in seconds
    */
    inline double time_section() const
    {
        return t_section.count();
    }

  private:
    std::chrono::_V2::system_clock::time_point t0;
    std::chrono::duration<double> t_total, t_part, t_section;

    bool running;

    #ifdef SGP_COMPILE_WITH_MPI
    MPI_Comm comm; // MPI communicator
    bool sync;
    #endif // SGP_COMPILE_WITH_MPI
};

}// namespace sgp
