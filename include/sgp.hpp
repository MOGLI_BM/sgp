/*
    Skeleton implementation of sequential global programming (SPG) algorithm.
    SGP is an optimization algorithm for solving problems of the type
                        u = argmin_v J(v, P(v))
    with P(u) being the solution of A(u)P(u) = B

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#pragma once
#include <iostream>
#include <vector>
#include <array>
#include <stdexcept>
#include <string>
#include <limits>
#include <cmath>
#include <memory>
#include <cassert>
#include <iomanip>


#include "mpi_tools.hpp"

#ifdef SGP_COMPILE_WITH_MPI
    #include <mpi.h>
#endif // SGP_COMPILE_WITH_MPI


#include "parametrization.hpp"
#include "result.hpp"
#include "timer.hpp"

namespace sgp {

template <class T>
using Arr1D = std::vector<T>;

/* wrapper for 2d arrays, i.e., Arr1D<Arr1D<T>>
*/
template <class T>
struct Arr2D : public Arr1D<Arr1D<T>>
{
    Arr2D()
        : Arr1D<Arr1D<T>>()
    {}

    Arr2D(size_t rows, size_t cols)
        : Arr1D<Arr1D<T>>(rows, Arr1D<T>(cols))
    {}

    Arr2D(size_t rows, size_t cols, const T& initVal)
        : Arr1D<Arr1D<T>>(rows, Arr1D<T>(cols, initVal))
    {}

    Arr2D(const Arr1D<Arr1D<T>>& arr)
        : Arr1D<Arr1D<T>>(arr)
    {}

    Arr2D(Arr1D<Arr1D<T>>&& arr)
        : Arr1D<Arr1D<T>>(arr)
    {}


    inline size_t rows() const {return this->size();}
    inline size_t cols() const {return (*this)[0].size();}
};

/* wrapper for LINEARIZED 3d arrays, i.e., Arr1D<T>(planes * rows * columns)
*/
template <class T>
struct Arr3D : public Arr1D<T>
{
    Arr3D()
        : Arr1D<T>()
    {}

    Arr3D(size_t planes, size_t rows, size_t cols)
        : Arr1D<T>(planes * rows * cols)
        , P(planes), R(rows), C(cols), RC(R * C)
    {}

    Arr3D(size_t planes, size_t rows, size_t cols, const T& initval)
        : Arr1D<T>(planes * rows * cols, initval)
        , P(planes), R(rows), C(cols), RC(R * C)
    {}

    inline const T& operator()(size_t i, size_t j, size_t k) const
    {
        return (*this)[RC * i + C * j + k];
    }

    inline T& operator()(size_t i, size_t j, size_t k)
    {
        return (*this)[RC * i + C * j + k];
    }

    inline size_t planes() const {return P;}
    inline size_t rows() const {return R;}
    inline size_t cols() const {return C;}

  private:
    size_t P, R, C, RC;
};

enum class StoppingCriterion
{
    ENERGY,         // stop algorithm when |j_new - j_old| < tol
    CONFIGURATION   // stop algorithm when \|U_new - U_old\| < tol
};

// declarations
template <class MAT_t, class SOL_t, class MOD_1_t, class MOD_2_t, int INTERPOLATION>
class Algorithm;

class Eval_JS;

template <class MAT_t, class SOL_t, class MOD_1_t, class MOD_2_t, int INTERPOLATION>
Arr1D<Eval_JS> gradient_check_S(Algorithm<MAT_t, SOL_t, MOD_1_t, MOD_2_t, INTERPOLATION>& alg, size_t edge_idx, int k1, int k2, size_t n);


/* Abstract base class of SPG-algorithm
    @tparam MAT_t           data-type of material parameters
    @tparam SOL_t           data-type of solution vector for underlying problem
    @tparam MOD_t           data-type of load parameter
    @tparam INTERPOLATION   type of interpolation scheme for edges of material graph, e.g. INTERPOLATION=1 for linear interpolation
*/
template <class MAT_t, class SOL_t, class MOD_1_t, class MOD_2_t, int INTERPOLATION>
class Algorithm
{
  public:
    using edge_t    = std::array < size_t, INTERPOLATION + 1 >;
    using coeff_t   = std::array < MAT_t, INTERPOLATION + 1 >;

    /* Ctor (sets default stopping criteria)
        @param  N_          number of (local) degrees of freedom
        @param  materials_  vector of admissible materials, i.e., the nodes in the material-graph
                            Note: each material is given by a vector of material properties where the k-th
                                entry corresponds to the k-th load parameter of first kind, i.e., mods_1_[k]
        @param  edges_      edges of graph of admissible materials; an edge is defined by INTERPOLATION+1 node-indices.
        @param  mods_1_     vector of k1 load parameters of first kind, i.e., load parameters, which also change the material parameters
        @param  mods_2_     vector of k2 load parameters of second kind, i.e., load parameters, which don't change material parameters
        @param  dd_         Domain decomposition information -- only available if compiled with MPI
        @param  verbose_    if true, state information will be printed to std::out in each iteration
    */
    Algorithm(const size_t N_
              , const Arr2D<MAT_t>& materials_, const Arr1D<edge_t>& edges_, const Arr1D<MOD_1_t>& mods_1_, const Arr1D<MOD_2_t>& mods_2_
              #ifdef SGP_COMPILE_WITH_MPI
              , const mpi::DomainDecomposition dd_
              #endif // SGP_COMPILE_WITH_MPI
              , const bool verbose_ = false)
        : Algorithm(Arr1D<bool>(N_, true), materials_, edges_,  mods_1_, mods_2_
                    #ifdef SGP_COMPILE_WITH_MPI
                    , dd_
                    #endif // SGP_COMPILE_WITH_MPI
                    , verbose_)
    {}

    /* Ctor (sets default stopping criteria)
        @param  to_optimize vector of flags s.th. to_optimize[k] specifies whether the k-th node shall be
                            optimized or not (i.e. it keeps its initial value)
        @param  materials_  vector of admissible materials, i.e., the nodes in the material-graph
                            Note: each material is given by a vector of material properties where the k-th
                                entry corresponds to the k-th load parameter of first kind, i.e., mods_1_[k]
        @param  edges_      edges of graph of admissible materials; an edge is defined by INTERPOLATION+1 node-indices.
        @param  mods_1_     vector of k1 load parameters of first kind, i.e., load parameters, which also change the material parameters
        @param  mods_2_     vector of k2 load parameters of second kind, i.e., load parameters, which don't change material parameters
        @param  dd_         Domain decomposition information -- only available if compiled with MPI
        @param  verbose_    if true, state information will be printed to std::out in each iteration
    */
    Algorithm(const Arr1D<bool> to_optimize
              , const Arr2D<MAT_t>& materials_, const Arr1D<edge_t>& edges_, const Arr1D<MOD_1_t>& mods_1_, const Arr1D<MOD_2_t>& mods_2_
              #ifdef SGP_COMPILE_WITH_MPI
              , const mpi::DomainDecomposition dd_
              #endif // SGP_COMPILE_WITH_MPI
              , const bool verbose_ = false)
        : flag(to_optimize), N(to_optimize.size()), materials(materials_), edges(edges_), mods1(mods_1_), mods2(mods_2_)
          #ifdef SGP_COMPILE_WITH_MPI
        , dd(dd_)
          #endif // SGP_COMPILE_WITH_MPI
        , verbose(verbose_), n_edges(edges_.size()), n_mod1(mods_1_.size()), n_mod2(mods_2_.size())
          #ifdef SGP_COMPILE_WITH_MPI //==============================================
        , idx_dof(mpi::compute_local_indices(dd.mod.np, N))
        , idx_mod1(mpi::compute_local_indices(dd.mod.np_1, n_mod1))
        , idx_mod2(mpi::compute_local_indices(dd.mod.np_2, n_mod2))
        , idx_dof_loc(idx_dof[dd.mod.rnk])
        , idx_mod1_loc(idx_mod1[dd.mod.coord[0]])
        , idx_mod2_loc(idx_mod2[dd.mod.coord[1]])
        , mod_coords(dd.mod.np)
          #else //===========================================================
        , idx_dof(mpi::compute_local_indices(1, N))
        , idx_mod1(mpi::compute_local_indices(1, n_mod1))
        , idx_mod2(mpi::compute_local_indices(1, n_mod2))
        , idx_dof_loc(idx_dof[0])
        , idx_mod1_loc(idx_mod1[0])
        , idx_mod2_loc(idx_mod2[0])
        , mod_coords(1)
          #endif //============================================== SGP_COMPILE_WITH_MPI
        , u_old(n_mod1, N),         u_new(n_mod1, N)
        , result_old(N),            result_new(N)
        , J_old(n_mod1, n_mod2),    J_new(n_mod1, n_mod2)
        , edge_coeffs(n_edges, n_mod1)
    {
        P = Arr2D<Arr1D<SOL_t>>(idx_mod1_loc.size, idx_mod2_loc.size, Arr1D<SOL_t>(N));
        Q = Arr2D<Arr1D<SOL_t>>(idx_mod1_loc.size, idx_mod2_loc.size, Arr1D<SOL_t>(N));

        #ifdef SGP_COMPILE_WITH_MPI //==============================================

        // tinitialize timing tree with MPI communicator
        for (size_t ts = 0; ts < N_TIMER_SECTIONS; ++ts)
        {
            timingTree[ts] = Timer(dd.world.comm);
        }

        lossTimer = Timer(dd.world.comm, false);

        /* initialize internal copies of solution vectors used for sgp algorithm,
            i.e., the ordering compared to P/Q is reversed. Furthermore, the Arrays
            are split up for better access in MPI communication: P_T[rank](dof,mod1,mod2)
        */
        for (int rnk = 0; rnk < dd.mod.np; ++rnk)
        {
            mod_coords[rnk] = dd.mod.rank_to_coords(rnk);
            int n_mod1_rnk =  idx_mod1[mod_coords[rnk][0]].size;
            int n_mod2_rnk =  idx_mod2[mod_coords[rnk][1]].size;

            P_T.push_back(Arr3D<SOL_t>(idx_dof_loc.size, n_mod1_rnk, n_mod2_rnk));
            Q_T.push_back(Arr3D<SOL_t>(idx_dof_loc.size, n_mod1_rnk, n_mod2_rnk));
        }

        #else

        mod_coords[0] = {0, 0};

        P_T = Arr1D(1, Arr3D<SOL_t>(N, n_mod1, n_mod2));
        Q_T = Arr1D(1, Arr3D<SOL_t>(N, n_mod1, n_mod2));

        #endif //============================================== SGP_COMPILE_WITH_MPI

        for (auto& mat : materials)
        {
            if (mat.size() != n_mod1)
            {
                throw std::invalid_argument(
                    "Material Description Error: Each material must have exacly one value for each load parameter!");
            }
        }

        compute_coefficients();

        setParams(StoppingCriterion::ENERGY, 1e-16, std::numeric_limits<int>::max(), 0.0, std::numeric_limits<int>::max(),
                  1e-16, 1e-16, 4);
    }

    ~Algorithm() {}

    /* define parameters (stopping criteria etc.) for optimizaiton
        (Ctor sets default values (ENERGY, 1E-16, MAX_INT, 0.0, MAX_INT, 1E-16, 1E-16, 4))
        @param  type_               convergence measure for outer loop: either |u_old-u_new| or |j_old-j_new|
        @param  tol_                convergence tolerance for outer loop
        @param  max_iter_outer_     maximum iterations until aborting outer loop
        @param  min_improvement_    minimal improvement of new solution to be accepted, i.e., inner loop stops when j_new + min_improvement_ < j_old
        @param  max_iter_inner_     maximum iterations for inner loop until old result is assumed to be the minimum
        @param  eps_rho_            minimum parameter range for edge multi-section
        @param  eps_s_              tolerance for s within edge multi-section
        @param  n_section_          number of points for edge multi-section including boundary nodes (>= 4)
    */
    void setParams(const StoppingCriterion type_, const double tol_, const size_t max_iter_outer_, const double min_improvement_, const size_t max_iter_inner_, const double eps_rho_, const double eps_s_, const size_t n_section_)
    {
        if (tol_ < 0 || min_improvement_ < 0 || eps_rho_ < 0 || eps_s_ < 0)
        {
            throw std::invalid_argument("stoppingCriteria: tolerances must be positive!");
        }

        if (max_iter_outer_ < 0)
        {
            throw std::invalid_argument("stoppingCriteria: max iterations must not be negative!");
        }

        if (n_section_ < 4)
        {
            throw std::invalid_argument("setParams: n_section_ must not be less than 4!");
        }

        convergence_type        = type_;
        convergence_tol         = tol_;
        maxIterations_outer     = max_iter_outer_;
        minImprovement          = min_improvement_;
        maxIterations_inner     = max_iter_inner_;
        eps_rho                 = eps_rho_;
        eps_s                   = eps_s_;
        n_section               = n_section_;
    }

    /* apply algorithm with same initial material for all DoF
      @param  tau         reciprocal parameter step size
      @param  nu_plus     increase factor for tau in inner loop: nu_plus > 1
      @param  nu_minus    decrease factor for tau in outer loop: nu_minus < 1
      @param  omega       penalty factor: J_pen(v(r)) = omega*(r*(1-r))
      @param  u0          initial design u[i] = materials[u0]
      @return             solution as vector of node_index/grayness pairs
    */
    Arr1D<Result> solve(double tau, const double nu_plus, const double nu_minus, const double omega, const int u0 = 0)
    {
        Arr1D<size_t> uvec(N);
        std::fill(uvec.begin(), uvec.end(), u0);

        return solve(tau, nu_plus, nu_minus, omega, uvec);
    }

    /* apply algorithm with initial design according to u0
        @param  tau         reciprocal parameter step size
        @param  nu_plus     increase factor for tau in inner loop: nu_plus > 1
        @param  nu_minus    decrease factor for tau in outer loop: nu_minus < 1
        @param  omega       penalty factor: J_pen(v(r)) = omega*(r*(1-r))
        @param  u0          initial design u[i] = materials[u0[i]]
        @return             solution as vector of node_index/grayness pairs
    */
    Arr1D<Result> solve(double tau, const double nu_plus, const double nu_minus, const double omega, const Arr1D<size_t>& u0)
    {
        if (tau < 0)
        {
            throw std::invalid_argument("tau must be non-negative!");
        }

        if (nu_plus <= 1)
        {
            throw std::invalid_argument("nu_plus must be greater than 1!");
        }

        if (nu_minus >= 1 || nu_minus <= 0)
        {
            throw std::invalid_argument("nu_minus must be in range (0,1)!");
        }

        if (nu_minus * nu_plus >= 1)
        {
            throw std::invalid_argument("nu_minus must be less than 1/nu_plus!");
        }

        if (omega < 0)
        {
            throw std::invalid_argument("omega must be non-negative!");
        }

        for (auto& timer : timingTree)
        {
            timer.reset();
            timer.new_section();
        }
        lossTimer.reset();

        // initialize u
        get_configuration(u0, u_new);

        // initialize result vector
        timingTree[MISC].start();

        for (size_t i = 0; i < N; ++i)
        {
            result_new[i] = {u0[i], 0};
        }

        timingTree[MISC].stop();

        // initial solution
        double j_pen = 0.0;
        double j_reg = 0.0;

        j_new = eval_all(u_new, P, mods1, mods2, j_pen, J_new);

        // initialize "old" values
        timingTree[MISC].start();

        u_old = u_new;

        result_old = result_new;

        J_old = J_new;

        double tau_old = tau;

        timingTree[MISC].stop();

        printStartOutput();

        printStartline();

        printElement("Iteration", false, 10);

        printElement("J(u)", false, 21);

        printElement("omega*J_pen(u)", false, 21);

        const std::string convtype = (convergence_type == StoppingCriterion::ENERGY) ? "|j_new - j_old|" : "||u_new - u_old||";

        printElement(convtype, false, 21);

        printElement("inner iterations", false, 21);

        printElement("time", false, 21);

        printEndline();

        // iterate until convergence or until maxIterations_outer
        int iter_outer = 0;

        int iter_inner = 0;

        double conv_val = nan("1");

        double rel_loss_total = 0.0; // total efficiency of load balancing

        while (1)
        {
            // timing
            double t_iter = 0.0;

            for (auto& timer : timingTree)
            {
                t_iter += timer.time_section();
            }

            // print state of previous iteration
            printStartline();
            printElement(iter_outer, false, 10);
            printElement(j_new - j_pen, false, 21);
            printElement(j_pen, false, 21);
            printElement(conv_val, false, 21);
            printElement(iter_inner, false, 21);
            printElement(t_iter, false, 21);
            printEndline();

            // store state of previous iteration
            iter_info.push_back(IterInfo{iter_outer, j_new-j_pen, j_pen, j_reg, conv_val, iter_inner, tau_old, t_iter});
            tau_old = tau;

            if (conv_val < convergence_tol)
            {
                printLine("Algorithm converged!");
                break;
            }

            if (++iter_outer > maxIterations_outer)
            {
                printLine("ERROR: Algorithm not converged (Did not reach convergence tolerance within given maximum number of iterations)!");
                break;
            }

            for (auto& timer : timingTree)
            {
                timer.new_section();
            }

            // store values from previous iteration
            swap_new_old();
            // prepare data structures for new iteration
            timingTree[MISC].start();
            prepare_iteration();
            timingTree[MISC].stop();
            // solve adjoint problem
            eval_adj_all(u_old, Q, mods1, mods2);
            // reorder solution vectors for separable problem
            reorder_solution_vectors(P, P_T, 1);
            reorder_solution_vectors(Q, Q_T, 2);

            // loop until better configuration is found
            iter_inner = 0;

            do
            {
                if (++iter_inner > maxIterations_inner)
                {
                    // no better solution found -> return previous solution
                    printStartline();
                    printElement("Iteration ", true, 0);
                    printElement(iter_outer, true, 0);
                    printElement(": Inner loop reached max iterations -> keeping result from iteration ", true, 0);
                    printElement(iter_outer - 1, true, 0);
                    printEndline();

                    std::swap(u_new, u_old);
                    // result_new = grayness(result_old, u0);
                    result_new = result_old;
                    return finalize();
                }

                // set u_new = argmin_v S[u_old,tau](v)
                j_pen = minimize_S(tau, omega, j_reg);

                // solve problem with new configuration
                j_new = eval_all(u_new, P, mods1, mods2, j_pen, J_new);

                // increase reciprocal step-size
                tau *= nu_plus;
            }
            while (minImprovement >= (j_old - j_new));

            // decrease reciprocal step-size
            tau *= nu_minus;

            conv_val = convergence_value();
        }

        // result_new = grayness(result_new, u0);
        return finalize();
    }

    /* get J(u*) for optimal configuration u* (requires running solve() first)
        @return  J(u*), i.e., energy functional evaluated for current configuration,
                            summed up over all modifiers
    */
    double getJ() const
    {
        Arr2D<Arr1D<SOL_t>> tmpP(idx_mod1_loc.size, idx_mod2_loc.size, Arr1D<SOL_t>(N));
        Arr2D<double> J(n_mod1, n_mod2);

        // for (auto& u : u_old[0])
        // {
        //     std::cerr << u << std::endl;
        // }
        return eval_all(u_new, tmpP, mods1, mods2, 0.0, J);
    }

    /* get J(u*) for optimal configuration u* (requires running solve() first)
        @return  J(u*), i.e., energy functional evaluated for current configuration
                s.th. J[i][j] corresponds to mods1[i] and mods2[j]
    */
    Arr2D<double> getJvec() const
    {
        Arr2D<Arr1D<SOL_t>> tmpP(idx_mod1_loc.size, idx_mod2_loc.size, Arr1D<SOL_t>(N));
        Arr2D<double> J(n_mod1, n_mod2);
        eval_all(u_new, tmpP, mods1, mods2, 0.0, J);
        return J;
    }

    /* compute J(u) for a given material configuration
        @param  u       material configuration as vector of material indices
        @return         J(u), i.e., energy functional evaluated for given configuration,
                            summed up over all modifiers
    */
    double computeJ(const Arr1D<size_t>& u) const
    {
        Arr2D<MAT_t> tmpU(n_mod1, N);
        get_configuration(u, tmpU);

        Arr2D<Arr1D<SOL_t>> tmpP(idx_mod1_loc.size, idx_mod2_loc.size, Arr1D<SOL_t>(N));
        Arr2D<double> J(n_mod1, n_mod2);

        return eval_all(tmpU, tmpP, mods1, mods2, 0.0, J);
    }

    /* compute J(u) for a given mono-material configuration
        @param  u       material index
        @return         J(u), i.e., energy functional evaluated for given configuration,
                            summed up over all modifiers
    */
    double computeJ(const size_t u) const
    {
        Arr1D<size_t> uvec(N);
        std::fill(uvec.begin(), uvec.end(), u);

        return computeJ(uvec);
    }

    /* compute local memory usage of internal data structures
        @return local memory usage in bytes
    */
    virtual size_t local_memory_usage()
    {
        size_t total = 0;

        total += u_old.rows()*u_old.cols()*sizeof(MAT_t);
        total += u_new.rows()*u_new.cols()*sizeof(MAT_t);
        total += materials.rows()*materials.cols()*sizeof(MAT_t);
        total += P.rows()*P.cols()*N*sizeof(SOL_t);
        total += Q.rows()*Q.cols()*N*sizeof(SOL_t);

        for (size_t i = 0; i < P_T.size(); ++i)
        {
            total += P_T[i].size()*sizeof(SOL_t);
            total += Q_T[i].size()*sizeof(SOL_t);
        }

        total += mods1.size()*sizeof(MOD_1_t);
        total += mods2.size()*sizeof(MOD_2_t);
        total += edges.size()*sizeof(edge_t);
        total += edge_coeffs.rows()*edge_coeffs.cols()*sizeof(coeff_t);

        total += J_old.rows()*J_old.cols()*sizeof(double);
        total += J_new.rows()*J_new.cols()*sizeof(double);

        total += result_old.size()*sizeof(Result);
        total += result_new.size()*sizeof(Result);

        total += mod_coords.size()*sizeof(std::array<int,2>);
        total += idx_dof.size()*sizeof(mpi::LocalIndex);
        total += idx_mod1.size()*sizeof(mpi::LocalIndex);
        total += idx_mod2.size()*sizeof(mpi::LocalIndex);

        return total;
    }

    /* return information about all iterations of the optimization
    */
    const std::vector<IterInfo>& get_iter_info() const
    {
        return iter_info;
    }

  private:

    // container class for trial material on a parametrized edge of the material graph
    struct Trial
    {
        Trial(const size_t edge_index, const size_t edge_dim) : edge_idx(edge_index), v(edge_dim)
        {}

        size_t          edge_idx;
        double          rho;    // parameter s.th. e(rho) = v
        Arr1D<MAT_t>   v;      // trial material
        double          s;      // S[u](v)
        double          j_pen;  // J_pen(rho)*omega
    };

    /* convert optimization result to node_idx and grayness
        @param  edge_rho    vector of edge_idx/rho pairs
        @param  initial_value vector of initial node indices
        @returns    vector of material_idx/grayness pairs with
                        grayness in [0,1]
    */
    Arr1D<Result> grayness(const Arr1D<Result>& edge_rho, const Arr1D<size_t>& initial_value)
    {
        Arr1D<Result> result(N);

        for (size_t i = 0; i < N; ++i)
        {
            if (flag[i])
            {
                // compute node on edge which is closest to edge(rho)
                size_t node = std::lround(edge_rho[i].val * INTERPOLATION);
                // get corresponding material index = edges[edge_idx][node]
                result[i].idx = edges[edge_rho[i].idx][node];
                // compute grayness = 4*J_pen(rho)
                result[i].val = 4 * J_pen(edge_rho[i].val);
            }
            else
            {
                result[i] = {initial_value[i], 0.0};
            }
        }

        return result;
    }

    /* convert optimization result to material_idx and grayness
    */
    Result grayness(size_t edge_idx, double rho)
    {
        Result mat;

        // compute node on edge which is closest to rho
        size_t node = std::lround(rho * INTERPOLATION);
        // get corresponding material index = edges[edge_idx][node]
        mat.idx = edges[edge_idx][node];
        // compute grayness = 4*J_pen(rho)
        mat.val = 4 * J_pen(rho);

        return mat;
    }

    /* @return current value of predefined convergece criterion,
        i.e., either ||u_new-u_old|| or |j_new-j_old|
    */
    inline double convergence_value() const
    {
        timingTree[MISC].start();

        double val = 0;

        if (convergence_type == StoppingCriterion::ENERGY)
        {
            val = j_new - j_old;
            val *= val;
        }

        if (convergence_type == StoppingCriterion::CONFIGURATION)
        {
            // todo implement

            throw std::runtime_error("StoppingCriterion::CONFIGURATION not implemented!");

            // msg = "||u_new - u_old||";

            // compute ||u_new - u_old||^2
            // for (size_t k = 0; k < n_mod; ++k)
            for (int k1 = idx_mod1_loc.begin; k1 < idx_mod1_loc.end; ++k1)
            {
                //! u only variates in mod1 not mod2 -> only one of the dd.mod.np_2 processes should contribute here
                // for (int k2 = idx_mod2_loc.begin; k2 < idx_mod2_loc.end; ++k2)
                // {
                for (size_t i = 0; i < N; ++i)
                {
                    MAT_t diff = u_new[k1][i] - u_old[k1][i];
                    val += norm2(diff);
                }

                // }
            }

            val = sum_global(val);
        }

        val = std::sqrt(val);

        timingTree[MISC].stop();

        return val;
    }

    /* compute u_new = argmin_v S[u_old, tau](v)
        @param  tau         penalty factor for |u_old-v|^2
        @param  omega       penalty factor for grayness
        @param  j_reg       reference where new value for j_reg shall be stored
        @returns J_pen(u_new)*omega
    */
    double minimize_S(const double tau, const double omega, double& j_reg)
    {
        timingTree[OPTIMIZE_S].start();

        double j_pen_new = 0; // update j_pen

        Arr2D<double> tmp_S(n_mod1, n_mod2);
        Arr2D<double> tmp_J_reg(n_mod1, n_mod2);
        double j_reg_new = 0;

        int rnk = 0;
        int np = 1;
        #ifdef SGP_COMPILE_WITH_MPI
        rnk = dd.mod.rnk;
        np = dd.mod.np;
        #endif // SGP_COMPILE_WITH_MPI

        // loop over all DoF
        for (int i_loc = 0; i_loc < idx_dof_loc.size; ++i_loc)
        {
            int i = i_loc + idx_dof_loc.begin;

            if (!flag[i]) continue;

            double s_i = std::numeric_limits<double>::infinity(); // global minimum
            double j_pen_i, j_reg_i, j_reg_trial;

            // loop over all edges
            for (size_t edge_idx = 0; edge_idx < edges.size(); ++edge_idx)
            {
                const auto& e = edge_coeffs[edge_idx];

                auto min            = std::make_shared<Trial>(edge_idx, n_mod1); // optimum on current edge
                auto trial          = std::make_shared<Trial>(edge_idx, n_mod1); // trial materials
                min->s              = std::numeric_limits<double>::infinity(); // value of S[u](min->v) =: s_e
                double s_max;

                double rho_l = 0.0;
                double rho_r = 1.0;

                // find s_e = min_{v in e} S[u](v)
                do
                {
                    // max_{rho in [rho_l,rho_r]} S[u_i](v_i(rho)) (required for stopping crit)
                    s_max = -std::numeric_limits<double>::infinity();
                    // rho increment for current range [rho_l,rho_r]
                    double rho_incr = (rho_r - rho_l) / double(n_section - 1);

                    // edge multi-section
                    for (int n = 0; n < n_section; ++n)
                    {
                        // compute rho
                        trial->rho = rho_l + n * rho_incr;

                        // compute penalty
                        trial->j_pen = omega * J_pen(trial->rho);

                        // interpolate v and compute S for each load parameter
                        // loop over partitions of mod1/mod2
                        for (int prt = 0; prt < np; ++prt)
                        {
                            const mpi::LocalIndex& idx1 = idx_mod1[mod_coords[prt][0]];
                            const mpi::LocalIndex& idx2 = idx_mod2[mod_coords[prt][1]];

                            for (int k1_prt = 0; k1_prt < idx1.size; ++k1_prt)
                            {
                                int k1 = k1_prt + idx1.begin;

                                // interpolate v
                                Parametrization::eval(e[k1], trial->v[k1], trial->rho);

                                for (int k2_prt = 0; k2_prt < idx2.size; ++k2_prt)
                                {
                                    int k2 = k2_prt + idx2.begin;

                                    tmp_S[k1][k2] = S(u_old[k1][i], trial->v[k1], P_T[prt](i_loc, k1_prt, k2_prt), Q_T[prt](i_loc, k1_prt, k2_prt), trial->j_pen, mods1[k1], mods2[k2], tau, i, k1, tmp_J_reg[k1][k2]);
                                }
                            }
                        }

                        trial->s = gradSumJ(J_old, tmp_S, mods1, mods2);

                        // new minimum?
                        if (trial->s < min->s)
                        {
                            std::swap(trial, min);
                            j_reg_trial = gradSumJ(J_old, tmp_J_reg, mods1, mods2);
                        }
                        // new maximum? (required for stopping crit)
                        else if (trial->s > s_max)
                        {
                            s_max = trial->s;
                        }
                    }

                    // update range
                    rho_l = std::max(rho_l, min->rho - rho_incr);
                    rho_r = std::min(rho_r, min->rho + rho_incr);
                }
                while ((rho_r - rho_l) > eps_rho && (s_max - min->s) > eps_s);

                if (min->s < s_i)
                {
                    j_pen_i = min->j_pen;
                    j_reg_i = j_reg_trial;
                    s_i = min->s;

                    // store current minimum as new solution
                    // result_new[i] = {min->edge_idx, min->rho};
                    result_new[i] = grayness(min->edge_idx, min->rho);

                    for (size_t k = 0; k < n_mod1; ++k)
                        u_new[k][i] = min->v[k];
                }
            }

            j_pen_new += j_pen_i;
            j_reg_new += j_reg_i;
        }

        timingTree[OPTIMIZE_S].stop();

        // todo don't store u_old and u_new
        // mpi communication
        #ifdef SGP_COMPILE_WITH_MPI //==============================================
        timingTree[MISC].start();
        MPI_Datatype MPI_RESULT_TYPE, MPI_MAT_TYPE;
        MPI_Type_contiguous(sizeof(Result), MPI_BYTE, &MPI_RESULT_TYPE);
        MPI_Type_contiguous(sizeof(MAT_t), MPI_BYTE, &MPI_MAT_TYPE);
        MPI_Type_commit(&MPI_RESULT_TYPE);
        MPI_Type_commit(&MPI_MAT_TYPE);

        for (int prt = 0; prt < dd.mod.np; ++prt)
        {
            MPI_Bcast(&(result_new[idx_dof[prt].begin]), idx_dof[prt].size, MPI_RESULT_TYPE, prt, dd.mod.comm);

            for (int k1 = 0; k1 < n_mod1; ++k1)
            {
                MPI_Bcast(&(u_new[k1][idx_dof[prt].begin]), idx_dof[prt].size, MPI_MAT_TYPE, prt, dd.mod.comm);
            }
        }

        MPI_Type_free(&MPI_RESULT_TYPE);
        MPI_Type_free(&MPI_MAT_TYPE);

        timingTree[MISC].stop();

        #endif //============================================== SGP_COMPILE_WITH_MPI

        j_reg = sum_global(j_reg_new);
        return sum_global(j_pen_new);
    }

    /* sum up the local sums of all MPI-processes
        @param  localsum    the local sum on this process
        @return             sum_{k=0,...,np-1} localsum_k
    */
    double sum_global(const double& localsum) const
    {

        double tmp = localsum;
        #ifdef SGP_COMPILE_WITH_MPI
        timingTree[MISC].start();

        // printElement("\nallreduce\n", true, 0);
        MPI_Allreduce(&localsum, &tmp, 1, MPI_DOUBLE, MPI_SUM, dd.world.comm);

        timingTree[MISC].stop();
        #endif // SGP_COMPILE_WITH_MPI

        return tmp;
    }

    /* update old values with new ones
    */
    void swap_new_old()
    {
        timingTree[MISC].start();

        std::swap(u_old,        u_new);
        std::swap(result_old,   result_new);
        std::swap(J_old,        J_new);
        j_old           =       j_new;

        timingTree[MISC].stop();
    }

    /* print final stats and return result
        @return             solution as vector of edge_index/rho pairs
    */
    Arr1D<Result> finalize() const
    {
        double t_eval = timingTree[EVALUATE].time_total();
        double t_misc = timingTree[MISC].time_total();
        double t_opt = timingTree[OPTIMIZE_S].time_total();

        // evaluate time loss to measure quality of load balancing
        double rel_loss = (t_eval - lossTimer.time_total()) / t_eval;
        rel_loss = sum_global(rel_loss);
        #ifdef SGP_COMPILE_WITH_MPI
        rel_loss /= dd.world.np;
        #endif

        double t_total = 0.0;

        for (auto& timer : timingTree)
        {
            t_total += timer.time_total();
        }

        printEndOutput();

        printLine("Wall-clock time in seconds:");

        printStartline();
        printElement("Total time to solution (tts):", true, 46);
        printElement(t_total, false, 12);
        // printElement(" s", false, 0);
        printEndline();

        printStartline();
        printElement("Time for evaluating P(u):", true, 46);
        printElement(t_eval, false, 12);
        printElement(" = ", false, 0);
        printElement(t_eval / t_total, false, 0);
        printElement(" * tts  ", false, 0);
        printEndline();

        printStartline();
        printElement("Time for optimization (incl. solving S[u]):", true, 46);
        printElement(t_opt, false, 12);
        printElement(" = ", false, 0);
        printElement(t_opt / t_total, false, 0);
        printElement(" * tts  ", false, 0);
        printEndline();

        printStartline();
        printElement("Additional time for data handling:", true, 46);
        printElement(t_misc, false, 12);
        printElement(" = ", false, 0);
        printElement(t_misc / t_total, false, 0);
        printElement(" * tts  ", false, 0);
        printEndline();

        printStartline();
        printElement("Efficiency of load balancing, i.e. proportion of non-idle time during eval()-step:", true, 46);
        printElement(1 - rel_loss, false, 12);
        printEndline();

        return result_new;
    }

    /* reorder data obtained from calling eval_all to be suitable for separable approximation
        @param  M   idx_mod1_loc.size x idx_mod2_loc.size matrix of solution vectors of size N
        @param  M_T reference to the memory, where the "transposed" of M will be stored where
                    M_T is a vector of 3D arrays s.th. M_T[r](i_loc,j_loc,k_loc) corresponds
                    to rank r in dd.mod.comm and M[j_loc][k_loc][i],
                    i.e., M_T contains all mods but fewer dof than M
    */
    void reorder_solution_vectors(const Arr2D<Arr1D<SOL_t>>& M, Arr1D<Arr3D<SOL_t>>& M_T, int tag)
    {
        timingTree[MISC].start();

        assert(M.rows() == idx_mod1_loc.size && M.cols() == idx_mod2_loc.size);

        #ifdef SGP_COMPILE_WITH_MPI //==============================================

        Arr3D<SOL_t> tmp(N, M.rows(), M.cols());
        #else
        Arr3D<SOL_t>& tmp = M_T[0];
        assert(tmp.planes() == N && tmp.rows() == M.rows() && tmp.cols() == M.cols());

        #endif //============================================== SGP_COMPILE_WITH_MPI

        // reordering
        for (int k1_loc = 0; k1_loc < idx_mod1_loc.size; ++k1_loc)
        {
            for (int k2_loc = 0; k2_loc < idx_mod2_loc.size; ++k2_loc)
            {
                assert(M[k1_loc][k2_loc].size() == N);

                for (int i = 0; i < N; ++i)
                {
                    tmp(i, k1_loc, k2_loc) = M[k1_loc][k2_loc][i];
                }
            }
        }

        // mpi communication
        #ifdef SGP_COMPILE_WITH_MPI //==============================================

        MPI_Datatype MPI_SOL_TYPE;
        MPI_Type_contiguous(sizeof(SOL_t), MPI_BYTE, &MPI_SOL_TYPE);
        MPI_Type_commit(&MPI_SOL_TYPE);

        Arr1D<MPI_Request> req(2 * dd.mod.np);
        Arr1D<MPI_Status> stat(2 * dd.mod.np);
        // const int tag = 1;

        for (int prt = 0; prt < dd.mod.np; ++prt)
        {
            SOL_t* data = &(tmp(idx_dof[prt].begin, 0, 0));
            int size = idx_dof[prt].size * M.rows() * M.cols();

            assert(M_T[prt].planes() == idx_dof_loc.size
                   && M_T[prt].rows() == idx_mod1[mod_coords[prt][0]].size
                   && M_T[prt].cols() == idx_mod2[mod_coords[prt][1]].size);

            MPI_Issend(data, size, MPI_SOL_TYPE, prt, tag, dd.mod.comm, &req[prt]);
            MPI_Irecv(M_T[prt].data(), M_T[prt].size(), MPI_SOL_TYPE, prt, tag, dd.mod.comm, &req[prt + dd.mod.np]);
        }

        MPI_Waitall(2 * dd.mod.np, req.data(), stat.data());

        MPI_Type_free(&MPI_SOL_TYPE);

        #endif //============================================== SGP_COMPILE_WITH_MPI

        timingTree[MISC].stop();
    }

    /* convert material indices to material configuration
    @param  u_idx   vector of material indices
    @param  u       matrix of material parameters s.th. u[k][i] corresponds to the k-th load
                    parameter and the i-th DoF.
    */
    void get_configuration(const Arr1D<size_t>& u_idx, Arr2D<MAT_t>& u) const
    {
        timingTree[MISC].start();

        if (u_idx.size() != N)
        {
            throw std::invalid_argument("u_idx must have exactly " + std::to_string(N) + " entries!");
        }

        for (size_t i = 0; i < N; ++i)
        {
            if (u_idx[i] < 0 || u_idx[i] >= materials.size())
            {
                throw std::invalid_argument("initial condition u_idx[" + std::to_string(i) + "] = " + std::to_string(u_idx[i]) + " is not part of the material graph!");
            }

            for (size_t k = 0; k < n_mod1; ++k)
            {
                u[k][i] = materials[u_idx[i]][k];
            }
        }

        timingTree[MISC].stop();
    }

    /* solve A(u)P(u) = B for all load parameters
        @param  u       material configuration
        @param  P       reference to solution vectors
        @param  m1      load parameters of first kind
        @param  m2      load parameters of second kind
        @param  j_pen_u J_pen(u)
        @param  J       matrix to store J(u_k) for all mods k=1,...,n_mod
        @return         sumJ( [J(u_k)+J_pen(u_k)]_{k=1,...,n_mod} )
    */
    double eval_all(const Arr2D<MAT_t>& u, Arr2D<Arr1D<SOL_t>>& P
                    , const Arr1D<MOD_1_t>& m1, const Arr1D<MOD_2_t>& m2
                    , const double j_pen_u, Arr2D<double>& J) const
    {
        timingTree[EVALUATE].start();
        lossTimer.start();

        // eval for all mod
        for (int k1_loc = 0; k1_loc < idx_mod1_loc.size; ++k1_loc)
        {
            int k1 = k1_loc + idx_mod1_loc.begin;

            for (int k2_loc = 0; k2_loc < idx_mod2_loc.size; ++k2_loc)
            {
                int k2 = k2_loc + idx_mod2_loc.begin;

                J[k1][k2] = eval(u[k1], P[k1_loc][k2_loc], m1[k1], m2[k2]);
            }
        }

        lossTimer.stop();
        timingTree[EVALUATE].stop();

        timingTree[MISC].start();

        int rnk = 0;
        int np = 1;
        #ifdef SGP_COMPILE_WITH_MPI
        rnk = dd.mod.rnk;
        np = dd.mod.np;
        #endif // SGP_COMPILE_WITH_MPI

        // j_mat = J + j_pen
        Arr2D<double> j_mat(n_mod1, n_mod2);

        for (int prt = 0; prt < np; ++prt)
        {
            const mpi::LocalIndex& idx1 = idx_mod1[mod_coords[prt][0]];
            const mpi::LocalIndex& idx2 = idx_mod2[mod_coords[prt][1]];

            for (int k1 = idx1.begin; k1 < idx1.end; ++k1)
            {
                for (int k2 = idx2.begin; k2 < idx2.end; ++k2)
                {
                    #ifdef SGP_COMPILE_WITH_MPI
                    MPI_Bcast(&(J[k1][k2]), 1, MPI_DOUBLE, prt, dd.mod.comm);
                    #endif // SGP_COMPILE_WITH_MPI

                    j_mat[k1][k2] = J[k1][k2] + j_pen_u;
                }
            }
        }

        double sum = sumJ(j_mat, m1, m2);

        timingTree[MISC].stop();

        return sum;
    }

    /* solve adjoint problem corresponding to A(u)P(u) = B for all load parameters
        @param  u       material configuration
        @param  Q       reference to solution vectors of adjoint problem
        @param  m       load parameters
    */
    void eval_adj_all(const Arr2D<MAT_t>& u, Arr2D<Arr1D<SOL_t>>& Q
                      , const Arr1D<MOD_1_t>& m1, const Arr1D<MOD_2_t>& m2) const
    {
        timingTree[EVALUATE].start();
        lossTimer.start();

        for (int k1_loc = 0; k1_loc < idx_mod1_loc.size; ++k1_loc)
        {
            int k1 = k1_loc + idx_mod1_loc.begin;

            for (int k2_loc = 0; k2_loc < idx_mod2_loc.size; ++k2_loc)
            {
                int k2 = k2_loc + idx_mod2_loc.begin;

                eval_adj(u[k1], Q[k1_loc][k2_loc], m1[k1], m2[k2]);
            }
        }

        lossTimer.stop();
        timingTree[EVALUATE].stop();
    }

  protected:

    /* implement in derived class if necessary */
    virtual void prepare_iteration(){}

    /* solve A(u)P(u) = B -- to be implemented in derived class.
        @param  u       material configuration
        @param  P       reference to solution vector
        @param  m1      load parameter of first kind
        @param  m2      load parameter of second kind
        @return         J(u), i.e., energy functional evaluated for current configuration
    */
    virtual double eval(const Arr1D<MAT_t>& u, Arr1D<SOL_t>& P, const MOD_1_t& m1, const MOD_2_t& m2) const = 0;

    /* solve adjoint problem corresponding to A(u)P(u) = B -- to be implemented in derived class.
        @param  u       material configuration
        @param  Q       reference to solution vector of adjoint problem
        @param  m1      load parameter of first kind
        @param  m2      load parameter of second kind
    */
    virtual void eval_adj(const Arr1D<MAT_t>& u, Arr1D<SOL_t>& Q, const MOD_1_t& m1, const MOD_2_t& m2) const = 0;

    /* compute i-th term (i=0,...,N-1) of problem specific separable approximation S, i.e., an approximation to dJ/du_i -- to be implemented in derived class.
        @param  u_i     i-th component of current material-vector
        @param  v_i     i-th component of test-vector
        @param  P_i     i-th component of solution
        @param  Q_i     i-th component of solution of adjoint problems
        @param  J_pen   penalty for grayness of v_i
        @param  m1      load parameter of first kind
        @param  m2      load parameter of second kind
        @param  tau     penalty factor for |u-v|^2
        @param  i       local index in {0,...,N-1}
        @param  k1      global index of mod1 in {0,...,n_mod1 - 1}
        @param  j_reg   storage for regularization term
    */
    virtual double S(const MAT_t& u_i, const MAT_t& v_i, const SOL_t& P_i, const SOL_t& Q_i
                     , const double J_pen_v_i, const MOD_1_t& m1, const MOD_2_t& m2, const double tau, const int i, const int k1, double& j_reg) const = 0;

    /* compute the i-th term of the scalar product u*u -- to be implemented in derived class.
        @ param u_i     i-th component of u
        @ return i-th term of u*u
    */
    virtual double norm2(const MAT_t& u_i) const = 0;

    /* rule for computing total energy J(u) based on the partial results J(u_mod)
            for each load parameter. -- to be implemented in derived class
        @param J    matrix of partial results s.th. J[j][k] is associated with mv1[j] and mv2[k]
        @param mv1  vector of load parameters of first kind
        @param mv2  vector of load parameters of first kind
        @return     (weighted) sum of the components J_k
    */
    virtual double sumJ(const Arr2D<double>& J, const Arr1D<MOD_1_t>& mv1, const Arr1D<MOD_2_t>& mv2) const = 0;

    /* rule for computing the gradient of sumJ, required for summing up S[u](vi) -- to be implemented in derived class
        @param J    matrix of partial results s.th. J[j][k] is associated with mv1[j] and mv2[k]
        @param dJ   matrix of partial results for dJ/du_i, s.th. dJ[j][k] is associated with mv1[j] and mv2[k]
        @param mv1  vector of load parameters of first kind
        @param mv2  vector of load parameters of first kind
        @return     (weighted) sum of the components s_k
    */
    virtual double gradSumJ(const Arr2D<double>& J, const Arr2D<double>& dJ, const Arr1D<MOD_1_t>& mv1, const Arr1D<MOD_2_t>& mv2) const = 0;

    /* compute penalty for grayness of configuration
        @param  rho   parameter s.th. u_i = interpolate(edge, rho) for some edge
        @return penalty J_pen(rho(u_i)) = grayness/4
    */
    inline double J_pen(const double rho) const
    {
        // for all rho s.th. rho*INTERPOLATION is integer, e(rho) is a node
        double tmp = rho * INTERPOLATION;
        tmp -= std::floor(tmp);

        return tmp * (1 - tmp);
    }

    /* compute coefficients for edge parametrization from edge data and store results in edge_coeffs
    */
    void compute_coefficients()
    {
        timingTree[MISC].start();

        // loop over all edges
        for (int i = 0; i < n_edges; ++i)
        {
            const edge_t& e = edges[i]; // edge: node indices
            coeff_t e_val; // values of edge nodes

            // loop over all first kind load parameters
            for (int k = 0; k < n_mod1; ++k)
            {
                for (int j = 0; j <= INTERPOLATION; ++j)
                {
                    // check validity of edge
                    if (e[j] >= materials.size())
                    {
                        throw std::invalid_argument("edge " + std::to_string(i) + " contains node " + std::to_string(e[j]) + ", which is not part of the material graph!");
                    }

                    e_val[j] = materials[e[j]][k];
                }

                Parametrization::interpolate(e_val, edge_coeffs[i][k]);
            }
        }

        timingTree[MISC].stop();
    }

    /* print information to std::out
        @param  elem    element to print
        @param  left    if true -> left-aligned, else -> right-aligned
        @param  width   fill element with spaces to width
    */
    template<typename T>
    void printElement(const T& elem, const bool left = true, const int width = 0) const
    {
        if (verbose)
        {
            if (left)
            {
                std::cout << std::left;
            }
            else
            {
                std::cout << std::right;
            }

            std::cout << std::setw(width) << std::setfill(' ') << elem;
            std::cout.flush();
        }
    }

    void printStartline() const
    {
        if (verbose) std::cout << ">>> ";
    }

    void printEndline() const
    {
        if (verbose)
        {
            std::cout << std::endl;
        }
    }

    void printLine(const std::string& msg) const
    {
        printStartline();
        printElement(msg, true, 0);
        printEndline();
    }

    void printStartOutput() const
    {
        if (verbose)
        {
            std::cout << std::scientific;
            std::cout.precision(3);
        }

    }

    void printEndOutput() const
    {
        if (verbose)
        {
            std::cout << std::defaultfloat;
            std::cout.precision(6);
        }

    }

    friend Arr1D<Eval_JS> gradient_check_S<MAT_t, SOL_t, MOD_1_t, MOD_2_t, INTERPOLATION>(Algorithm& alg, size_t edge_idx, int k1, int k2, size_t n);

    using Parametrization = parametrization::Polynomial<INTERPOLATION>;

    const Arr1D<bool>          flag;               // optimize only DoF where flag[k]==true
    const size_t                N;                  // number of DoF
    const size_t                n_mod1;             // number of load parameter of first kind
    const size_t                n_mod2;             // number of load parameter of second kind
    const size_t                n_edges;            // number of considered edges in graph of admissible materials
    // todo don't store u_old and u_new
    Arr2D<MAT_t>               u_old;              // material configuration from last iteration
    Arr2D<MAT_t>               u_new;              // material configuration from current iteration
    Arr2D<MAT_t>               materials;          // vector of admissible materials

    /* matrix of solution vectors from current iteration s.th.
        P[j][k][i] corresponds to the j-th/k-th mod1/mod2 from the set of
        mods on this MPI-rank and the i-th dof
    */
    Arr2D<Arr1D<SOL_t>>       P;

    /* matrix of adjoint solution vectors from current iteration s.th.
        Q[j][k][i] corresponds to the j-th/k-th mod1/mod2 from the set of
        mods on this MPI-rank and the i-th dof
    */
    Arr2D<Arr1D<SOL_t>>       Q;

    /* vector of solution matrices from previous iteration s.th.
        P_T[r](i,j,k) corresponds to the i-th dof on the restricted
        dof-set (s. transpose()), and mod1[j], mod2[k] from rank r
    */
    Arr1D<Arr3D<SOL_t>>       P_T;

    /* vector of adjoint solution matrices from previous iteration s.th.
        Q_T[r](i,j,k) corresponds to the i-th dof on the restricted
        dof-set (s. transpose()), and mod1[j], mod2[k] from rank r
    */
    Arr1D<Arr3D<SOL_t>>       Q_T;

    Arr1D<MOD_1_t>             mods1;              // vector of load parameters of first kind
    Arr1D<MOD_2_t>             mods2;              // vector of load parameters of second kind
    Arr1D<edge_t>              edges;              // vector of edge-data (defining nodes)
    Arr2D<coeff_t>             edge_coeffs;        // vector of coefficients for edge interpo   ion

    double                     j_old;              // sumJ( J(u_old) + J_pen(u_old) )
    double                     j_new;              // sumJ( J(u_new) + J_pen(u_new) )
    Arr2D<double>              J_old;              // J_old[j][k] = J(u_old, mod1[j], mod2[k])
    Arr2D<double>              J_new;              // J_new[j][k] = J(u_new, mod1[j], mo k])

    bool                       verbose;            // print useful information to st out

    std::vector<IterInfo> iter_info;               // information about all iterations

    StoppingCriterion          convergence_type;
    double                     convergence_tol;
    size_t                     maxIterations_outer;
    size_t                     n_section;
    double                     minImprovement;
    size_t                     maxIterations_inner;
    double                     eps_rho;
    double                     eps_s;

    Arr1D<Result>              result_old;         // vector of edge index and parameter rho for each DoF corresponding to u_old
    Arr1D<Result>              result_new;         // vector of edge index and parameter rho for each DoF corresponding to u_new

    #ifdef SGP_COMPILE_WITH_MPI
    const mpi::DomainDecomposition  dd; // MPI communicators, ranknumbers, number of processes, etc.
    #endif // SGP_COMPILE_WITH_MPI

    Arr1D<std::array<int, 2>> mod_coords; // coords for each rank in dd.mod.

    // local indices for restricted access to mod1 (parallel access) running from 0 to dd.mod.np_1
    Arr1D<mpi::LocalIndex>         idx_mod1;
    // local indices for restricted access to mod2 (parallel access) running from 0 to dd.mod.np_2
    Arr1D<mpi::LocalIndex>         idx_mod2;
    // local indices for restricted access to dof (for serial mod access) running from 0 to dd.mod.np
    Arr1D<mpi::LocalIndex>         idx_dof;

    // = idx_mod1[dd.mod.coord[0]]
    mpi::LocalIndex                 idx_mod1_loc;
    // = idx_mod2[dd.mod.coord[1]]
    mpi::LocalIndex                 idx_mod2_loc;
    // = idx_dof[dd.mod.rnk]
    mpi::LocalIndex                 idx_dof_loc;

    static constexpr size_t N_TIMER_SECTIONS = 3;
    enum TimerSections {OPTIMIZE_S, EVALUATE, MISC};

    mutable std::array<Timer, N_TIMER_SECTIONS> timingTree; // time measurement for algorithm
    mutable Timer lossTimer; // measure lost time due to insufficient load balancing

};

} // namespace sgp
