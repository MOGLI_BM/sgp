/*
    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

namespace sgp {

// container to store optimization result data for one DoF
struct Result
{
   size_t idx; // edge or node index, depending on usage
   double val; // parametrization coordinate rho or grayness

   friend std::ostream& operator<<(std::ostream& out, const Result& result)
   {
      out << result.idx << "    " << result.val;
      return out;
   }
};

struct IterInfo
{
   int    iteration; // iteration number
   double J;         // J(u_iter)
   double J_pen;     // J_pen(u_iter)
   double J_reg;     // J_reg(u_old, u_new)
   double delta_j;   // stopping criterion, i.e., |j_new - j_old| or ||u_new - u_old||
   int    n_inner;   // number of inner iterations, i.e., nuber of subproblems
   double tau;       //
   double time;      // total time for this iteration

   // print data in csv style
   friend std::ostream& operator<<(std::ostream& out, const IterInfo& result)
   {
      out << result.iteration << ", " //
          << result.J << ", "         //
          << result.J_pen << ", "     //
          << result.J_reg << ", "     //
          << result.delta_j << ", "   //
          << result.n_inner << ", "   //
          << result.tau << ", "       //
          << result.time;

      return out;
   }
};

} // namespace sgp