

file = sprintf('../../data/gradientcheck.dat')
stats file;
set grid lc 'gray'
set xlabel "ϱ(v_i)"
# set logscale x
# set ytics 10 format '$10^{%L}$'
# set xtics 1


set term wxt 0 size 800,800 pos 0,0;

set ylabel "function values"

plot \
    file u 1:2 w l ls 1 title "J(u-u_i+v_i)",\
    file u 1:3 w l ls 2 title "S[u](v_i)"
