"""
    codegeneration for parametrization.cpp

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
"""

import numpy as np


def setup_matrix(q):
    """ setup vandermode matrix

        @param  q   polynomial degree

        @return A   Vandermonde matrix
    """
    A = np.zeros([q+1,q+1])

    # loop over columns
    for k in range(q+1):
        # k-th basis function
        def p(t):
            return t**k

        # loop over rows
        for i in range(q+1):
            rho = i/q
            A[i,k] = p(rho)

    return A


def sign_to_string(a):
    if np.sign(a) < 0:
        return " - "
    else:
        return " + "

def matrixentry_to_string(a):
    result = sign_to_string(a)
    if abs(a) == 1:
        return result
    else:
        return result + "%f * " %(abs(a))

def print_setup(q):
    """ print function parametrization::Polynomial<q>::setup

        @param  q   polynomial degree
    """
    A = setup_matrix(q)
    Ai = np.linalg.inv(A)

    contents = ""

    for i in range(q+1):

        contents += "    coeffs[%d] =" %(i)

        for k in range(q+1):
            a = Ai[i, k]
            if a:
                contents += matrixentry_to_string(a) + "data[%d]" %(k)

        contents += ";\n"

    print("template <>\ntemplate <class T>")
    print("inline void Polynomial<%d>::setup(const Nodes<T>& data, Nodes<T>& coeffs)\n{\n%s}" %(q,contents))

print_setup(3)
